# -*- coding: utf-8 -*-
"""
frozen dir 冻结路径
执行打包后的程序，经常会出现程序使用的图标无法显示，程序使用的关联文件无法关联。或者，在打包的本机上运行正常，但是将打包后的程序放到其它机器上就有问题。这些现象都很有可能是由程序使用的文件路径发生改变产生的，因此在打包时候我们需要根据执行路径进行路径“冻结”。
"""
import sys
import os

def app_path():
    """Returns the base application path."""
    if hasattr(sys, 'frozen'):
        # Handles PyInstaller
        return os.path.dirname(sys.executable)
    return os.path.dirname(__file__)