# chia 的绘图管理器 
 

![The view of the manager](https://i.imgur.com/SmMDD0Q.png "View")

开发版本：v0.0.1

这个库的目的是管理你的 chia 尽兴绘图，并使用你配置的 config.yaml 置来启动新的绘图。每个人的系统都不尽相同，所以定制是这个库中的一个重要特征（编写大家自己的 config.yaml）。

这个库很简单，易于使用，而且可靠，可以保持绘图的生成。

## 特点

错开你的绘图，这样你的计算机资源就可以避免高峰期。
允许一个目标目录列表。
通过交错时间提前启动一个新的绘图，最大限度地利用临时空间。
同时运行最大数量的绘图，以避免瓶颈或限制资源占用。
更深入的检测绘制过程。

## 所有命令

##### 命令的使用实例
```文字
> python3 manager.py start

> python3 manager.py restart

> python3 manager.py stop

> python3 manager.py view

> python3 manager.py status

> python3 manager.py analyze_logs
```

### start

这个命令将在后台启动管理器。一旦你启动它，它就会一直运行，除非所有的作业都完成了`max_plots` 或者出现了错误。错误将被记录在一个创建的`debug.log`文件中。

### stop

这个命令将在后台终止管理器，它不会停止运行中的绘图，只会停止新绘图的创建。

### restart

该命令将依次运行启动和停止。

### view

该命令将显示你可以用来跟踪你正在运行的绘图的视图。这将在你的`config.yaml'中定义的每X秒更新一次。

### status

该命令将对视图进行一次快照,它不会循环。

### analyze_logs

该命令将分析你的日志文件夹中所有完成的绘图日志，并为你的计算机配置计算适当的权重。只需在你的`config.yaml`中的`progress`部分填入返回的值。这只影响进度条。


## 使用方法

1、按照你自己的个人设置编辑和设置`config.yaml` 下面有更多关于这方面的帮助

2、运行管理器： 鼠标双击`start.bat`

- 这将在后台启动一个进程，根据你输入的设置来管理绘图。

3、运行视图： 鼠标双击`view.bat`

- 这将循环查看检测屏幕上的正在活动地块的详细信息。

4、停止管理器： 鼠标双击`stop.bat`

## 配置

这个库的配置对每个终端用户都是独一无二的 `config.yaml` 文件是配置的所在。

这个绘图管理器是基于任务的概念来工作的。每个任务都有自己的设置，你可以对每个任务进行个性化设置，没有哪个是唯一的，所以这将为你提供灵活性。

### 管理

这些是只由绘制管理器使用的配置设置。

- `check_interval` - 在检查是否有新任务开始之前的等待秒数。

- `log_level` - 保持在 ` ERROR ` 上，只在有错误时记录。把它改为 `INFO`，以便看到更详细的日志记录。警告:`INFO` 会写下大量的信息。

### 日志

- `folder_path` - 这是你的日志文件的文件夹，用于保存绘图。

### 视图

这些是视图将使用的设置

- `check_interval` - 更新视图前的等待秒数。
- `datetime_format` - 视图中希望显示的日期时间格式。格式化见这里：[https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes)
- `include_seconds_for_phase` - 时间转换格式是否包括秒。
- `include_drive_info` - 是否会显示驱动器相关信息。
- `include_cpu` - 是否显示CPU的相关信息。
- `include_ram` - 是否显示RAM的相关信息。
- `include_plot_stats` - 是否会显示绘图统计相关信息。
### 通知
这些是不同的设置，以便在绘图管理器启动和绘图完成时发送通知。

### 进度
- `phase_line_end` - 这些设置将用于决定一个阶段在进度条中的结束时间。它应该反映出该阶段结束的线，这样进度计算就可以使用该信息与现有的日志文件来计算进度百分比。
- `phase_weight` - 这些是在进度计算中分配给每个阶段的权重,通常情况下，第1和第3阶段是最长的阶段，所以它们将比其他阶段拥有更多的权重。

### 全局
- `max_concurrent` - 你的系统可以运行的最大绘图数量,管理器在一段时间内启动的地块总数不会超过这个数量。
- `max_for_phase_1` - 系统在第一阶段可以运行的最大绘图数量。
- `minimum_minutes_between_job` - 开始一个新的绘图任务前的最小分钟数，这可以防止多个工作在同一时间开始，这将缓解目标磁盘的拥堵，设置为0表示禁用。

## 任务
- 这些是每个任务使用的设置，请注意，你可以有多个任务，每个任务都应该是YAML格式的，这样才能正确配置。这里几乎所有的值都将被传递到Chia可执行文件中。

点击这里参考更多关于Chia CLI的详情：[https://github.com/Chia-Network/chia-blockchain/wiki/CLI-Commands-Reference](https://github.com/Chia-Network/chia-blockchain/wiki/CLI-Commands-Reference)

- `name` - 这是你要给的名字。
- `max_plots` - 这是在管理器的一次运行中，任务的最大数量。任何重新启动管理器的操作都会重置这个变量，它在这里只是为了帮助你在这段时间内的绘图。
- [OPTIONAL] `farmer_public_key` - 你的chia耕种公钥。如果没有提供，它将不会把这个变量传给chia执行程序，从而导致你的默认密钥被使用。只有当你在一台没有你的证书的机器上设置了chia时才需要这个。
- [OPTIONAL] `pool_public_key` - 你的池公钥。与上述信息相同。
- `temporary_directory` - 这里应该只传递一个目录。这是将进行绘图的地方。
- [OPTIONAL]`temporary2_directory `- 可以是一个单一的值或一个值的列表。这是一个可选的参数，如果你想使用 Chia 绘图的 temporary2 目录功能，可以使用这个参数。
- `destination_directory` - 可以是一个单一的值或一个值的列表。这是绘图完成后将被转移到的最终目录。如果你提供一个列表，它将逐一循环浏览每个磁盘。
- `size` - 这指的是绘图的k大小。你可以在这里输入32、33、34、35......这样的内容（这取决于你之前的习惯）
- `bitfield` - 这指的是你是否想在你的绘图中使用`bitfield`通常情况下，推荐使用 `true`
- `threads` - 这是将分配给 plot 绘图的线程数。只有第1阶段使用1个以上的线程。（这里尤为注意，这是每个绘图任务的线程，对应 chia 官方的 2 自行改动）
- `buckets` - 要使用的桶的数量。Chia提供的默认值是128。
- `memory_buffer` - 你想分配给进程的内存数量。
- `max_concurrent` - 这个任务在任何时候都要有的最大数量的绘图。
- `max_concurrent_with_start_early` - 这项工作在任何时候拥有的最大绘图数量，包括提前开始的阶段。
- `stagger_minutes` - 每个任务并发之间的交错时间单位 分钟。如果你想让你的 plot 在并发限制允许的情况下立即被启动，你甚至可以把它设置为零（为 0 就是同步开始，没有交错时间，不推荐设置为0，最佳 stagger 一般是平均速度的 1/6为最佳，这是我在 reddit 看到的测试）
- `max_for_phase_1` - 这个任务在第1阶段的最大绘图数量。
- `concurrency_start_early_phase` - 你想提前启动一个绘图的阶段。建议使用4。
- `concurrency_start_early_phase_delay` - 当检测到提前开始阶段时，在新的绘图被启动之前的最大等待分钟数。 

- `temporary2_destination_sync` - 这个字段将始终提交目标目录作为临时2目录。这两个目录将是同步的，因此它们将总是以相同的值提交。

- `exclude_final_directory` - 是否为收个几跳过最终目录

- `destination_directory`进行耕作，（这是Chia的一个功能）
- `skip_full_destinations` - 启用该功能时，它将计算所有正在运行的 plot 和未来 plot 的大小，以确定磁盘上是否有足够的空间来启动任务，如果没有，它将跳过该磁盘，转到下一个，一旦所有的空间都满了，它就会停用作业。
- `unix_process_priority` - 仅限UNIX操作系统，这是 plot 生成时将被赋予的优先级。UNIX值必须在-20和19之间。该值越高，进程的优先级越低。
- `windows_process_priority` - 仅限Windows操作系统，这是 plot 在生成时将被赋予的优先级。Windows的数值不同，应该设置为以下数值之一。
	- 16384 `below_normal_priority_class`（低于正常优先级）。
	- 32 `normal_priority_class`（正常优先级）。
	- 32768 "高于正常优先级"。
	- 128 "高优先级 "的
	- 256 "实时优先级"。
- `enable_cpu_affinity` - 启用或禁用绘图进程的 cpu 亲和性，绘图和收割的系统在排除一个或两个线程的绘图进程时，可能会看到收割机或节点性能的改善。
- `cpu_affinity` - 为绘图进程分配的cpu（或线程）的列表。默认例子假设你有一个超线程的4核CPU（8个逻辑核心）。这个配置将限制绘图进程使用逻辑核心0-5，把逻辑核心6和7留给其他进程（6个使用（限制6个），2个空闲）。